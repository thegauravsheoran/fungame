import React, { useState } from 'react';
import axios from 'axios';
import { Table } from 'react-bootstrap';

const Form = () => {
  const [name, setName] = useState('');
  const [isValid, setIsValid] = useState(false);
  const [result, setResult] = useState([]);

  const handleNameChange = (e) => {
    const inputName = e.target.value;
    // Validate the name field
    const regex = /^[a-zA-Z\s]*$/;
    const isValidName = regex.test(inputName) && inputName.trim().split(/\s+/).length <= 2;
    setIsValid(isValidName && inputName.length >= 3);
    setName(inputName);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const apiUrl = `https://api.agify.io/?name=${name}`;
    const { data: { age } } = await axios.get(apiUrl);
    const apiUrl2 = `https://api.nationalize.io/?name=${name}`;
    const { data: { country } } = await axios.get(apiUrl2);
    const apiUrl3 = `https://api.genderize.io/?name=${name}`;
    const { data: { gender } } = await axios.get(apiUrl3);

    const countryArr = country.slice(0, 2).map(({ country_id, probability }) => `${country_id}(${probability.toFixed(2)})`);
    setResult([{ name, gender, age, country: countryArr.join(' or ') }]);
  };

  const handleReset = () => {
    setName('');
    setIsValid(false);
    setResult([]);
  };

  return (<>
    <div className="form">
      <form onSubmit={handleSubmit}>
        <label htmlFor="name">Name: </label>
        <input type="text" id="name" name="name" value={name} onChange={handleNameChange} />
        {!isValid && <p className="error">Please enter a valid name with at least 3 characters and no more than 2 spaces.</p>}
        <button type="submit" disabled={!isValid}>Check Fun</button>
        {result.length > 0 && (
          <>
            <Table  cellPadding="50px" align='center' border="3px solid black" >
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Gender</th>
                  <th>Age</th>
                  <th>Country</th>
                </tr>
              </thead>
              <tbody>
                {result.map(({ name, gender, age, country }) => (
                  <tr key={name}>
                    <td>{name}</td>
                    <td>{gender}</td>
                <td>{age}</td>
                <td>{country}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <button type="button" onClick={handleReset}>Clear</button>
      </>
    )}
  </form>
</div>

</>
);
};

export default Form;


