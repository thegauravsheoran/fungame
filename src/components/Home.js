import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => {
  return (
    <div className="home">
      <Link to="/form">
        <button className="btn-start">Start Fun</button>
      </Link>
    </div>
  );
};

export default Home;
