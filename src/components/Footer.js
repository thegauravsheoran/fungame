import React from 'react';

const Footer = () => {
  return (
    <div className="footer">
      <p>© MatrIoT</p>
    </div>
  );
};

export default Footer;
